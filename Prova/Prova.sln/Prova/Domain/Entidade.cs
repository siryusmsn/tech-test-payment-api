﻿using Flunt.Notifications;

namespace Prova.Domain
{
    public abstract class Entidade : Notifiable<Notification>
    {
        public Entidade()
        {
        }
        public int Id { get; set; }
        public string Nome { get; set; }
       
    }
}
