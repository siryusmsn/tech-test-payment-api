﻿namespace Prova.Domain.Vendas
{
    public class Venda : IVenda
    {

        public int Id { get; set; }
        public string Status { get; set; }
        public DateTime DataVenda { get; set; }
        public Vendedor Vendedor { get; set; }
        public List<ListaVenda> Produtos { get; set; }
        public double? VendaTotal { get; set; }

        public Venda()
        {
            Vendedor = new Vendedor();
            Produtos = new List<ListaVenda>();
            Status = "Aguardando pagamento";
        }

        public Venda(int id, Vendedor vendedor, List<ListaVenda> produtos, double? vendaTotal)
        {
            Id = id;
            DataVenda = DateTime.Now;
            Vendedor = vendedor;
            Produtos = produtos;
            Status = "Aguardando pagamento";
            VendaTotal = vendaTotal;
        }

        public Venda(Vendedor vendedor, List<ListaVenda> produtos)
        {
            DataVenda = DateTime.Now;
            Vendedor = vendedor;
            Produtos = produtos;
            Status = "Aguardando pagamento";
        }
    }

  
}

