﻿namespace Prova.Domain.Vendas
{
    public interface IListaVenda
    {
        int Idvenda { get;}
        int Quantidade { get; set; }
        int IdProduto { get; set; }
    }
}
