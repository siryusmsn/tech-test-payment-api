﻿namespace Prova.Domain.Vendas
{
    public interface IVenda
    {
        int Id { get; set; }
        string Status { get; set; }
        DateTime DataVenda { get; set; }
        Vendedor Vendedor { get; set; }
        List<ListaVenda> Produtos { get; set; }
        double? VendaTotal { get; set; }
    }
}
