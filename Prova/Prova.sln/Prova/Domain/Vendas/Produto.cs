﻿using Flunt.Validations;
using System.Xml.Linq;

namespace Prova.Domain.Vendas
{
    public class Produto : Entidade, IProduto
    {
        public double Valor { get; set; }
        public Produto()
        {
        }

        public Produto(int id, string nome, double valor)
        {
            Id = id;
            Nome = nome;
            Valor = valor;
            Validate();          
        }

        private void Validate()
        {
            var contract = new Contract<Produto>()
                        .IsNotNullOrEmpty(Nome, "Nome", "Nome é obrigatório")
                        .IsGreaterOrEqualsThan(Nome, 3, "Name");
            AddNotifications(contract);
        }
    }
}
