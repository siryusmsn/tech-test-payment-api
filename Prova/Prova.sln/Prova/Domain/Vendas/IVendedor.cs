﻿namespace Prova.Domain.Vendas
{
    public interface IVendedor
    {
        string Cpf { get; set; }
        string Email { get; set; }
        string Telefone { get; set; }
    }
}
