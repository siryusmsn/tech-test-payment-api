﻿
using Flunt.Validations;

namespace Prova.Domain.Vendas
{
    public class Vendedor : Entidade, IVendedor
    {
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }

        public Vendedor()
        {
        }

        public Vendedor(int id, string nome, string cpf, string email, string telefone)
        {
            Cpf = cpf;
            Email = email;
            Telefone = telefone;
            Id = id == 0 ? 1 : id;
            Nome = nome;
            Validate();
        }
        private void Validate()
        {
            var contract = new Contract<Vendedor>()
                        .IsNotNullOrEmpty(Nome, "Nome", "Nome é obrigatório")
                        .IsGreaterOrEqualsThan(Nome, 3, "Name", "Precisa ter mais que três caracteres")
                        .IsNotNullOrEmpty(Cpf, "CPF", "CPF é obrigatório")
                        .IsGreaterOrEqualsThan(Cpf, 11, "CPF", "Precisa ter mais que 11 caracteres")
                        .IsNotNullOrEmpty(Email, "Email", "Email é obrigatório")
                        .IsEmail(Email, "Email")
                        .IsNotNullOrEmpty(Telefone, "Telefone", "Telefone é obrigatório")
                        .IsGreaterOrEqualsThan(Telefone, 11, "Telefone", "Precisa ter mais que 11 caracteres");
            AddNotifications(contract);
        }
    }
}
