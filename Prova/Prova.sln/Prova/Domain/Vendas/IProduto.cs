﻿namespace Prova.Domain.Vendas
{
    public interface IProduto
    {
        double Valor { get; set; }
    }
}
