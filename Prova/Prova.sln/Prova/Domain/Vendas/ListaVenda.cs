﻿namespace Prova.Domain.Vendas
{
    public class ListaVenda : IListaVenda
    {
        public int Idvenda { get; private set; }
        public int Quantidade { get; set; }
        public int IdProduto { get; set; }
        protected Venda Venda { get; private set; }
        protected Produto Produto { get; private set; }
        public string Nome { get; set; }
        public double Valor { get; set; }
        public ListaVenda()
        {
        }
        public ListaVenda(int idvenda, int quantidade , int idProduto, Venda venda, Produto produto)
        {
            Idvenda = idvenda;
            Quantidade = quantidade;
            IdProduto = idProduto;
            Venda = venda;
            Produto = produto;
            Nome = produto.Nome;
            Valor = produto.Valor;
        }
        public ListaVenda(int idProduto, int quantidade)
        {
            IdProduto = idProduto;
            Quantidade= quantidade;
        }
        public ListaVenda(int idProduto, int quantidade, Produto produto, int idVenda)
        {
            IdProduto = idProduto;
            Quantidade = quantidade;
            Produto = produto;
            Nome = produto.Nome;
            Idvenda = idVenda;
            Valor = produto.Valor;
        }
    }
}
