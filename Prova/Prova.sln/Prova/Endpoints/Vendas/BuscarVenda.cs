﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Prova.Infra.Dados;

namespace Prova.Endpoints.Vendas
{
    public class BuscarVenda
    {
        public static string Template => "/vendas/{idVenda}";
        public static string[] Methods => new string[] { HttpMethod.Get.ToString() };
        public static Delegate Handle => Action;

        public static async Task<IResult> Action([FromRoute] int idVenda)
        {
            IRepositoryVenda repositorio = new RepositoryVenda();
            var venda = repositorio.BuscarVenda(idVenda);
            if (venda != null)
                return Results.Ok(venda);
            return Results.NotFound();
        }
    }
}
