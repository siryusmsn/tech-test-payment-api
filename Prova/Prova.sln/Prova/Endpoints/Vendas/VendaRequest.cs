﻿using Prova.Domain.Vendas;

namespace Prova.Endpoints.Vendas
{
    public record VendaRequest(Vendedor vendedor, List<ListaVenda> Produtos);

}