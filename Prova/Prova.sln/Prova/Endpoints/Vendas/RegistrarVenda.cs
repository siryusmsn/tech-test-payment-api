﻿using Prova.Domain.Vendas;
using Prova.Infra.Dados;

namespace Prova.Endpoints.Vendas
{
    public class RegistrarVenda
    {
        public static string Template => "/vendas";
        public static string[] Methods => new string[] { HttpMethod.Post.ToString() };
        public static Delegate Handle => Action;

        public static async Task<IResult> Action(VendaRequest vendaRequest)
        {
            IRepositoryVenda repositorio = new RepositoryVenda();
            var vendedor = new Vendedor(vendaRequest.vendedor.Id, vendaRequest.vendedor.Nome, vendaRequest.vendedor.Cpf, vendaRequest.vendedor.Email, vendaRequest.vendedor.Telefone);
            if (!vendedor.IsValid)
                return Results.ValidationProblem(vendedor.Notifications.ConvertToProblemDetails());
            if(vendaRequest.Produtos != null)
            {
                foreach(var produto in vendaRequest.Produtos)
                {
                    Produto  produt = BaseDeDados.Produtos.FirstOrDefault(x => x.Id == produto.IdProduto);
                    if (produt == null)
                    {
                        produt = new Produto(produto.IdProduto, produto.Nome, produto.Valor);
                    }
                    if(!produt.IsValid)
                        return Results.ValidationProblem(produt.Notifications.ConvertToProblemDetails());
                }
            }

            var vendido = repositorio.RegistrarVenda(vendaRequest);

            if(vendido != null)
            return Results.Created($"/venda/{vendido.Id}", $"Venda nº:{vendido.Id} do vendedor {vendido.Vendedor.Nome} realizado com sucesso!");

            return Results.BadRequest("Voce nao passou um produto corretamente");
        }
    }
}
