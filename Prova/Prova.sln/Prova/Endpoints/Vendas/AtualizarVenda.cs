﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Prova.Infra.Dados;

namespace Prova.Endpoints.Vendas
{
    public class AtualizarVenda
    {
        public static string Template => "/vendas";
        public static string[] Methods => new string[] { HttpMethod.Put.ToString() };
        public static Delegate Handle => Action;

        public static IResult Action(int id,string status)
        {
            IRepositoryVenda repositorio = new RepositoryVenda();
            var venda = repositorio.AtualizarVenda(1, status);
            if(venda != null)
            {
                return Results.Ok($"Atualizado com sucesso para {status}");
            }
            return Results.BadRequest("Voce passou um status inválido.");
            
        }
    }
}
