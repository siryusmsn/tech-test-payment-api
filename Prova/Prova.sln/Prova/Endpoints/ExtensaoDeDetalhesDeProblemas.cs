﻿using Flunt.Notifications;
using Microsoft.AspNetCore.Identity;

namespace Prova.Endpoints;

public static class ProblemDetailsExtensions
{
    public static Dictionary<string, string[]> ConvertToProblemDetails(this IReadOnlyCollection<Notification> notificacoes)
    {
        return notificacoes
                .GroupBy(g => g.Key)
                .ToDictionary(g => g.Key, g => g.Select(x => x.Message).ToArray());
    }
}
