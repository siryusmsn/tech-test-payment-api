﻿using Prova.Domain.Vendas;
using Prova.Endpoints.Vendas;

namespace Prova.Infra.Dados
{
    public interface IRepositoryVenda
    {
        Venda AtualizarVenda(int idVenda, string status);
        Venda BuscarVenda(int idVenda);
        Venda RegistrarVenda(VendaRequest venda);
    }
}
