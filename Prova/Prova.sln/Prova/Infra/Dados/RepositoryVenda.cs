﻿using Prova.Domain.Vendas;
using Prova.Endpoints.Vendas;

namespace Prova.Infra.Dados
{
    public class RepositoryVenda : IRepositoryVenda
    {
        public Venda AtualizarVenda(int idVenda, string status)
        {
            var vendaSalva = BuscarVenda(idVenda);
            if (vendaSalva != null)
            {
                switch (vendaSalva.Status)
                {
                    case "Aguardando pagamento":
                        if (status.Contains("Pagamento Aprovado") || status.Contains("Cancelada"))
                        {
                            vendaSalva.Status = status;
                            return vendaSalva;
                        }
                        else
                            return null;
                        break;
                    case "Pagamento Aprovado":
                        if (status.Contains("Enviado para Transportadora") || status.Contains("Cancelada"))
                        {
                            vendaSalva.Status = status;
                            return vendaSalva;
                        }
                        else
                            break;
                    case "Enviado para Transportador":
                        if (status.Contains("Entregue"))
                        {
                            vendaSalva.Status = status;
                            return vendaSalva;
                        }
                        else
                            break;
                }
            }
            return null;
        }

        public Venda BuscarVenda(int idVenda)
        {
            return BaseDeDados.Vendas.FirstOrDefault(x => x.Id == idVenda);
        }

        public Venda RegistrarVenda(VendaRequest venda)
        {
            double preco;

            if (venda.Produtos == null)
                return null;
            else if (venda.Produtos[0].IdProduto == 0)
                return null;

            double total = 0;
            foreach (ListaVenda listaVenda in venda.Produtos)
            {
                Produto? produt = BaseDeDados.Produtos.FirstOrDefault(x => x.Id == listaVenda.IdProduto);
                if (produt == null)
                {
                    if (listaVenda.IdProduto != 0 && listaVenda.Nome != "string")
                    {
                        produt = new Produto(BaseDeDados.contadorIdProduto, listaVenda.Nome, listaVenda.Valor);
                        BaseDeDados.Produtos.Add(produt);
                        BaseDeDados.contadorIdProduto++;
                        preco = listaVenda.Valor * listaVenda.Quantidade;
                        total += preco;
                    }
                    else
                        return null;
                }                  
               else
                {
                    listaVenda.Valor = BaseDeDados.Produtos.FirstOrDefault(x => x.Id == listaVenda.IdProduto).Valor;
                    preco = listaVenda.Valor * listaVenda.Quantidade;
                    listaVenda.Nome = BaseDeDados.Produtos.FirstOrDefault(x => x.Id == listaVenda.IdProduto).Nome;
                    total += preco;
                }
            }

            var vendido = new Venda(BaseDeDados.contadorIdVenda, venda.vendedor, venda.Produtos, total);

            BaseDeDados.Vendas.Add(vendido);
            BaseDeDados.contadorIdVenda++;
            return vendido;
        }
    }
}
