﻿using Prova.Domain.Vendas;

namespace Prova.Infra.Dados
{
    public static class BaseDeDados
    {
        public static int contadorIdVenda { get; set; } = 2;
        public static int contadorIdProduto { get; set; } = 5;
        public static List<Produto> Produtos { get; set; } = new List<Produto> { new Produto(1, "Chocolare", 8), new Produto(2, "Biscoito", 7), new Produto(3, "Macarrao", 10), new Produto(4, "Carne", 12) };

        public static List<Venda> Vendas { get; set; } = new List<Venda> { new Venda(1, new Vendedor(1, "Marcelo", "10636945444", "marcelo@gmail.com", "49555641235"),
                new List<ListaVenda> {  new ListaVenda(3, 2, new Produto(3, "Macarrao", 10),1), new ListaVenda(4, 3, new Produto(4, "Carne", 12),1) }, 22) };

        
    }
}
