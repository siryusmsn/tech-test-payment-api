using Microsoft.OpenApi.Models;
using Prova.Endpoints.Vendas;
using Prova.Infra.Dados;

var builder = WebApplication.CreateBuilder(args);


// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Prova Pottencial  API",
        Version = "v1",
        Description = "API construída na prova da Pottencial",
        Contact = new OpenApiContact
        {
            Name = "Siryus Canuto Linkedin",
            Email = "siryuscanuto@gmail.com",
            Url = new Uri("https://www.linkedin.com/in/siryus-canuto-b7a9a4139/"),
        },
    });
  
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapMethods(RegistrarVenda.Template, RegistrarVenda.Methods, RegistrarVenda.Handle);
app.MapMethods(AtualizarVenda.Template, AtualizarVenda.Methods, AtualizarVenda.Handle);
app.MapMethods(BuscarVenda.Template, BuscarVenda.Methods, BuscarVenda.Handle);

app.Run();

